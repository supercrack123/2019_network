from threading import Thread
import time, platform, sys, os, socket, select, platform
import tkinter as tk
from tkinter import *
from tkinter.ttk import *
from tkinter.filedialog import askopenfile,Text
from tkinter.simpledialog import askstring

directory_split = '/'
if platform.system() == 'Windows':
    directory_split = '\\'
else:
    directory_split = '/'

current_directory = os.getcwd()
client_folder = current_directory + directory_split + 'client_folder'
if not (os.path.isdir(client_folder)):
    os.makedirs(os.path.join(client_folder))

HOST = '127.0.0.1'
PORT = 5058
checking = False
client_side = socket.socket()

PARAM = sys.argv
if len(PARAM) > 3:
    print("python3 [ip 주소] [포트번호] 로 입력해 주세요!!")
    exit()
elif len(PARAM) == 3:
    HOST= str(PARAM[1])
    PORT= int(PARAM[2])
elif len(PARAM) == 2:
    HOST= str(PARAM[1])
else:
    pass


client_side.connect((HOST,PORT))

def get_file(connection, filename):
    global checking
    global client_side

    time.sleep(3)
    result = connection.recv(4096).decode()
    time.sleep(3)
    if result == '_File_Download_':
        print("파일이 다운로드 중에 있습니다.")
    elif result == '_File_None_':
        checking = False
        print("파일이 존재하지 않습니다")
        return
    else:
        print("Error!!", result)
        return
    try:
        file = open('client_folder'+'/'+filename, "wb")
        while True:
            if checking == True:
                print(connection)
                prev = select.select([connection], [], [], 1)
                if not prev[0]:
                    break
                else:
                    result = connection.recv(4096)
                    if not result:
                        break
                    file.write(result)
        checking = False
        file.close()
    except Exception as e:
        print("Exception occured :",e)
        sys.exit(0)

    print("%s 다운로드가 완료되었습니다!"%filename)


def upload_file():

	global checking, client_side

	checking = True
	file= askopenfile(mode ='r', filetypes =[('All Files', '*.*')])

	if file is not None:
		print("파일 업로드 중입니다..")
		try:
			filename = file.name
			client_side.sendall('_file_upload_'.encode())
			time.sleep(2)
			client_side.sendall(filename.encode())
			result = ''

			with open(filename, "rb") as f:
				print("파일을 업로드하고 있는 중입니다!!!")
				result = f.read()
				client_side.sendall(result)

			print("%s 파일의 업로드가 완료되었습니다!"%filename)

		except Exception as e:
			print("파일업로드 중 다음과 같은 오류가 발생하였습니다.:",e)

	checking = False


def Receiving():
    global checking
    global client_side

    while True:
        for dummy in range(5000):
            pass
        if checking == False:
            prev = select.select([client_side], [], [], 1)
            if not prev[0]:
               continue
            else:
                try:
                    Message = client_side.recv(1024).decode()
                    if Message == '':
                        print("서버가 맛이 갔습니다...")
                        client_side.close()
                        supercrack.quit()
                    if len(Message.split(' ')) == 2 and Message.split(' ')[0] == '_get_file_':
                        checking = True
                        filename = Message.split(' ')[1]
                        get_file(client_side, filename)
                        continue
                    else:
                        chatting.insert(END, Message)
                except OSError as ose:
                    print("Exception in receive ",ose)
                    break
                except Exception as e:
                    print("Exception in receive ",e)
                    break
        else:
            pass

def Sending(event=None):
    global client_side
    global checking
    Message = wording.get()
    wording.set("")
    client_side.send(Message.encode())
    if Message == "/q":
        client_side.close()
        supercrack.quit()

def Closing(event=None):
    wording.set("/q")
    Sending()

supercrack = Tk()
supercrack.title("Supercrack Chatting Room")
supercrack.configure(background='white')
frame = Frame(supercrack)
wording = StringVar()
scroll = tk.Scrollbar(frame, bg='black')
chatting = tk.Listbox(frame, height=40, width=90, bg='green', fg='black', yscrollcommand=scroll.set)
scroll.pack(side=RIGHT,fill=Y)
chatting.pack(side=LEFT,fill=BOTH)
chatting.pack()
frame.pack()

file_upload_button = Button(supercrack, text ='FileUpload', command = lambda:upload_file())
file_upload_button.pack(side = RIGHT, pady = 15)

UI = Entry(supercrack,textvariable=wording)
UI.bind("<Return>", Sending)
UI.pack()
send_button = tk.Button(supercrack,text="메세지전송",bg='green',fg='black',command=Sending)
send_button.pack()
supercrack.protocol("WM_DELETE_WINDOW",Closing) # tkinter protocol

receive_thread = Thread(target=Receiving)
receive_thread.start()

if __name__ == '__main__':
    supercrack.mainloop()
