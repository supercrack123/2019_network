import errno, _io, socket, os, sys, platform, threading, select, time
from threading import Thread

directory_split = '/'

if platform.system() == 'Windows':
    directory_split = '\\'
else:
    directory_split = '/'

current_directory = os.getcwd()
server_directory = current_directory + directory_split + 'server_folder'
if not (os.path.isdir(server_directory)):
    os.makedirs(os.path.join(server_directory))

HOST = ''
PORT = 5058

connections_info = {}
address_info = {}
receiving = False
locking = threading.Lock()



PARAM = sys.argv
if len(PARAM) > 2:
    print("python3 [ip주소] [포트번호] 로 명령어를 재입력해주세요.")
    exit()
elif len(PARAM) == 2:
    PORT= int(PARAM[1])
else:
    pass

server = socket.socket()
server.bind((HOST,PORT))

def accepting():
    while True:
        connect, address = server.accept()
        connect.send(bytes("사용하시고 싶은 닉네임을 입력해주세요! -> ","utf-8"))
        print(address[0] + " 서버로 연결되었습니다!!" )
        address_info[connect] = address
        try:
            th = Thread(target=request_processing, args=(connect,))
            th.start()
        except Exception as e:
            print("exception ", e)
            th.stop()
        except IOError as ioe:
            if ioe.errno == 104:
                print("Exception occured ", ioe)
                th.stop()

def upFile(server,connect,filename):
    global receiving,locking

    try:
        filename = filename.split('/')[-1]
        f = open('server_folder'+'/'+filename, "wb")
        result_length = bytes()
        while True:
            if receiving == True:
                prev = select.select([connect], [], [], 1)
                if not prev[0]:
                   break
                else:
                    result = connect.recv(4096)
                    result_length+=result
                    if not result:
                        break
                    f.write(result)
        receiving = False
        f.close()
        print("%s 을 전송 받았습니다(업로드)!"%filename)


    except Exception as e:
        print("Exception occured",e)


def File_Sending(server,connect,filename):
    global receiving
    try:
        with open('server_folder' + '/' + filename, "rb") as f: # server 폴더에 있는 파일 중에서 다운로드
            connect.sendall("_File_Download_".encode())
            print("파일을 보내는 중에 있습니다...")
            result = f.read(4096)
            print(result)
            time.sleep(3)
            connect.sendall(result)
            while result:
                result = f.read(4096)
                connect.sendall(result)
    except Exception as e:
        connect.sendall("_File_None_".encode())
        receiving = False
        return None
    print(" %s 을 보냈습니다!"%filename)
    receiving = False

def request_processing(connect):
    try:
        clienid = connect.recv(1024).decode()
        print(clienid + "님께서 접속하셨습니다!!")
        if clienid == '/q':
            print("연결을 종료하고 있습니다...")
            connect.close()
            print("연결을 종료하였습니다!")
            send_message(bytes("%s 님이 채팅방을 나갔습니다 "%clienid,"utf-8"))
            return
        elif clienid == '':
            print("연결을 종료하고 있습니다...")
            connect.close()
            print("연결을 종료하였습니다!")
            send_message(bytes("%s 님이 채팅방에 입장하셨습니다 "%clienid,"utf-8"))
            return

        connect.send(("%s 님이  \n"%clienid).encode())
        connect.send(("\n Supercrack 채팅방에 입장하셨습니다.").encode())
        message = "%s 님이 채팅방에 입장하셨습니다."%clienid
        send_message(message.encode())
        connections_info[connect] = clienid
    except IOError as ioe:
        if ioe.errno == 104:
            print("Exception occured:",ioe)
            return
    while True:
        try:
            global receiving
            if receiving == False:
                prev = select.select([connect], [], [], 1)
                if not prev[0]:
                   continue
                else:
                    message = connect.recv(1024)
                    if message.decode() == '_file_upload_': # if correct, it's file transfer
                        receiving = True
                        filename = connect.recv(1024).decode()
                        upFile(server,connect,filename)

                    elif len(message.decode().split(' ')) == 2 and message.decode().split(' ')[0] == '/d':
                        receiving = True
                        filename = message.decode().split(' ')[1]
                        connect.send(("_get_file_"+" "+filename).encode())
                        File_Sending(server,connect,filename)

                    elif message.decode() != "/q":
                        print(message.decode())
                        if message.decode() == '':
                            print("메세지가 비어있습니다.")
                            check_connection = connect.recv(1024).decode()
                            print("연결상태 : ",check_connection)
                            if check_connection == '':
                                print("연결을 종료하고 있습니다...")
                                connect.close()
                                print("연결을 종료하였습니다!")
                                del connections_info[connect]
                                send_message(bytes("%s 님이 채팅방을 나갔습니다."%clienid,"utf-8"))
                                print("%s 님이 채팅방을 나갔습니다."%clienid)
                                return
                        send_message(message, " [" + clienid + "] ")
                    else:
                        print(message.decode())
                        connect.send("/q".encode())
                        print("연결을 종료하고 있습니다...")
                        connect.close()
                        print("연결을 종료하였습니다!")
                        del connections_info[connect]
                        send_message(bytes("%s 님이 채팅방을 나갔습니다"%clienid,"utf-8"))
                        break
        except IOError as ioe:
            if ioe.errno == 104:
                return
        except Exception as e:
            break

def send_message(message, first=""):
    for connections in connections_info:
        connections.send(bytes(first, "utf-8") + message)

if __name__ == "__main__":
    server.listen(40)
    supercrack = Thread(target=accepting)
    print("잠시 기다려주세요...")
    supercrack.start()
    supercrack.join()
    server.close()
